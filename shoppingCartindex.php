<?php include("./shoppingCartHeader.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?//php print_r($_SESSION['cart']) ?>
    <div class="row row-cols-2 row-cols-md-3 row-cols-lg-4 g-4 mt-1">
        <div class="col">
            <form action="manage_cart.php" method="POST">
                <div class="phoneCard card h-100">
                    <img src="../images/iphone11(1).webp" class="card-img-top" alt="..." />
                    <div class="card-body text-center">
                        <h5 class="card-title">Iphone11</h5>
                        <p class="card-text">
                            The ultimate product arrival.
                        </p>
                        <p class="text-danger">$1200</p>
                    </div>
                    <button type="submit" name="Add_To_Cart" class="btn btn-success">Add to cart</button>
                    <input type="hidden" name="Item_Name" value="Iphone11">
                    <input type="hidden" name="Price" value="1200">
                </div>
            </form>
        </div>
        <div class="col">
            <form action="manage_cart.php" method="POST">
                <div class="phoneCard card h-100">
                    <img src="../images/iphone11(1).webp" class="card-img-top" alt="..." />
                    <div class="card-body text-center">
                        <h5 class="card-title">Oppo</h5>
                        <p class="card-text">
                            The ultimate product arrival.
                        </p>
                        <p class="text-danger">$400</p>
                    </div>
                    <button type="submit" name="Add_To_Cart" class="btn btn-success">Add to cart</button>
                    <input type="hidden" name="Item_Name" value="Oppo">
                    <input type="hidden" name="Price" value="400">
                </div>
            </form>
        </div>
    </div>



</body>

</html>