<?php

// setup connection to database (databasename = phoneshop)
$pdo = new PDO('mysql:host=localhost; port=3306; dbname=phoneshop', 'root', '');
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


if (isset($_GET['id'])) {
  $id = $_GET['id'];
  $statement = $pdo->prepare("SELECT * FROM products WHERE id=:id");
  $statement->execute(['id' => $id]);
  $product = $statement->fetch();
}

//var_dump($product);
  $title = $product['title'];
  $price = $product['price'];
  $qty = 1;

  // if have no images folder, create folder name images
  if (!is_dir('images')) {
    mkdir('images');
  }
  // upload file
  $image = $_FILES['image']['name'] ? $_FILES['image']['name'] : $product['image'];
  $imagePath = '';
  // var_dump(file_exists($image), $image, $product['image']);
  // die();
  // check image exist or not
  if (!file_exists($image)) {
    $imagePath = 'images/' . randomString(8) . '/' . $_FILES['image']['name'];
    mkdir(dirname($imagePath));
    move_uploaded_file($_FILES['image']['tmp_name'], $imagePath);
  } else {
    $imagePath = $image;
  }

  $statement = $pdo->prepare("INSERT INTO shopping_cart(title, image, price, qty) VALUES(:title, :image, :price, :qty)");

  $statement->bindValue(':title', $title);
  $statement->bindValue(':image', $imagePath);
  $statement->bindValue(':price', $price);
  $statement->bindValue(':qty', $qty);
  $statement->execute();

// get random character
function randomString($n)
{
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $str = '';
  for ($i = 0; $i < $n; $i++) {
    $index = rand(0, strlen($characters) - 1);
    $str .= $characters[$index];
  }
  return $str;
}

?>

<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>add to cart</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
  <link rel="stylesheet" href="./style/app.css">
</head>

<body>
  <?php header('Location: showing_addToCart.php'); ?>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
</body>

</html>