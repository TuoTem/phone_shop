<?php

// connect to database
$pdo = new PDO('mysql:host=localhost; port=3306; dbname=phoneshop', 'root', '');
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$errors = [];
$alert = '';
$email = '';
$password = '';
$confirm_pass = '';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $email = $_POST['email'];
  $password = $_POST['password'];
  $confirm_pass = $_POST['confim_pass'];

  if(!$email) {
      $errors[] = "Email is required*";
  }
  if(!$password) {
      $errors[] = "Password is required*";
  }
  if(!$confirm_pass) {
    $errors[] = "Confirm-password is required*";
  }
  if($password != $confirm_pass) {
    $errors[] = "Password and Confirm password must be the same*";
  }
  if($email && $password && $confirm_pass && $password == $confirm_pass) {
    $statement = $pdo->prepare("INSERT INTO users(email, password) VALUES(:email, :password)");

    $statement->bindValue(':email', $email);
    $statement->bindValue(':password', $password);
    $statement->execute();

    $alert = "Account has been created";
  }

}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Signup Form</title>

    <!-- CSS -->
    <link rel="stylesheet" href="./style/loginSignup.css" />

    <!-- Boxicons CSS -->
    <link
      href="https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css"
      rel="stylesheet"
    />
  </head>
  <body>
    <section class="container forms">
      <!-- Sigup Form -->
      <div class="form login">
        <div class="form-content">
          <header>Signup</header>

          <!-- alert error -->
          <?php
          if(!empty($errors)) { ?>
          <div class="alert">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
            <?php foreach ($errors as $error) { ?>
                <div> <?php echo $error; ?> </div>
            <?php } ?>
          </div>
          <?php } ?>

          <!-- alert signup success -->
          <?php
          if(!empty($alert)) { ?>
          <div class="alert2">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
            <div> <?php echo $alert; ?> </div>
          </div>
          <?php } ?>

          <form action="" method="post">
            <div class="field input-field">
              <input type="email" placeholder="Email" class="input" name="email"/>
            </div>

            <div class="field input-field">
              <input type="password" placeholder="Password" class="password" name="password"/>
            </div>

            <div class="field input-field">
              <input type="password" placeholder="Confirm-Password" class="password" name="confim_pass"/>
              <i class='bx bx-hide eye-icon'></i>
            </div>

            <div class="field button-field">
              <button>Signup</button>
            </div>
          </form>

          <div class="form-link">
            <span
              >Already have an account?
              <a href="./LoginForm.php" class="link login-link">Login</a>
            </span>
          </div>
        </div>

        <div class="line"></div>

        <div class="media-options">
          <a href="https://web.facebook.com/login.php/?_rdc=1&_rdr" class="field facebook">
            <i class='bx bxl-facebook facebook-icon'></i>
            <span>Login with Facebook</span>
          </a>
        </div>

        <div class="media-options">
          <a href="https://accounts.google.com/v3/signin/identifier?dsh=S-805132891%3A1675273087075864&continue=https%3A%2F%2Fmyaccount.google.com%3Futm_source%3Daccount-marketing-page%26utm_medium%3Dcreate-account-button&flowName=GlifWebSignIn&flowEntry=AddSession" class="field google">
            <img src="../images/google.png" alt="" class="google-img">
            <span>Login with Google</span>
          </a>
        </div>
      </div>
    </section>

    <!-- Javascript -->
    <script src="./js/script.js"></script>
      
    </script>
  </body>
</html>
