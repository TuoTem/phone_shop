<?php

$pdo = new PDO('mysql:host=localhost; port=3306; dbname=phoneshop', 'root', '');
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$name = '';
$email = '';
$subject = '';
$message = '';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $name = $_POST['name'];
  $email = $_POST['email'];
  $subject = $_POST['subject'];
  $message = $_POST['message'];

  $statement = $pdo->prepare("INSERT INTO contactus(name,email,subject,message)
        VALUES(:name, :email, :subject, :message)");

  $statement->bindValue(':name', $name);
  $statement->bindValue(':email', $email);
  $statement->bindValue(':subject', $subject);
  $statement->bindValue(':message', $message);
  $statement->execute();
  
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Contact Us</title>

  <!-- CSS -->
  <link rel="stylesheet" href="./style/contactus.css" />

  <!-- Boxicons CSS -->
  <link href="https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css" rel="stylesheet" />

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous" />
</head>

<body>
  <!-- navbar -->
  <nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <!-- navbar-expand-md: when get to medium size screen, show actual navbar. But below medium size screen, show toggle button and drop down-->
    <div class="container-xxl">
      <!-- 100 width until extra extra large -->
      <a href="./index.php" class="navbar-brand">
        <p>PhoneShop</p>
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Toggle-navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- navbar-link -->
      <!-- class="data-bs-toggle for_styling_this_div " -->
      <div class="collapse navbar-collapse justify-content-center align-center" id="main-nav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="./phone.php">Phone</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./accessories.php">Accessories</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./about.php">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./contactus.php">Contact us</a>
          </li>
          <li class="nav-item d-md-none">
            <a class="nav-link" href="./LoginForm.php">log in</a>
          </li>
          <li class="nav-item d-md-none">
            <a class="nav-link" href="./signupForm.php">Sign Up</a>
          </li>
          <li class="nav-item ms-2 d-none d-md-inline">
            <!-- ms-2: margin start 2, d-none: on smaller screen, display none, d-md-inline: on medium screen, display inline -->
            <a class="btn btn-secondary" href="./LoginForm.php">Log In</a>
          </li>
          <li class="nav-item ms-2 d-none d-md-inline">
            <!-- ms-2: margin start 2, d-none: on smaller screen, display none, d-md-inline: on medium screen, display inline -->
            <a class="btn btn-success" href="./signupForm.php">Sign Up</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <section class="container forms">
    <!-- Login Form -->
    <div class="form login">
      <div class="form-content">
        <header>Contact Us</header>

        <form action="" method="post">
          <div class="field input-field">
            <input type="text" placeholder="Name" class="input" name="name" />
          </div>

          <div class="field input-field">
            <input type="email" placeholder="Email" class="input" name="email" />
          </div>

          <div class="field input-field">
            <input type="text" placeholder="Subject" class="input" name="subject" />
          </div>

          <div class="field input-field">
            <textarea placeholder="Message" name="message" rows="5" cols="45"></textarea>
          </div>

          <br> <br> <br>

          <div class="field button-field">
            <button type="submit">Send Message</button>
          </div>
        </form>
      </div>
  </section>

  <section id="footer">
    <div class="container-fluid bg-light py-5">
      <div class="row">
        <div class="col-12">
          <h3 class="mb-5 text-muted d-flex justify-content-center">FOLLOW US ON</h3>
          <div class="d-flex justify-content-center">
            <a href="">
              <img src="../images/facebook.png" alt="" width="25px" class="me-3">
            </a>
            <a href="" class="text-decoration-none">
              <p>Phone Shop</p>
            </a>
          </div>
          <div class="d-flex justify-content-center">
            <a href="">
              <img src="../images/instagram.png" alt="" width="25px" class="me-3">
            </a>
            <a href="" href="" class="text-decoration-none">
              <p>Phone Shop</p>
            </a>
          </div>
          <div class="d-flex justify-content-center">
            <a href="">
              <img src="../images/telegram.png" alt="" width="25px" class="me-3">
            </a>
            <a href="" class="text-decoration-none">
              <p>Phone Shop</p>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid text-center py-3">
      <p>Phone Shop © 2020. All Rights Reserved</p>
    </div>
  </section>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
</body>

</html>