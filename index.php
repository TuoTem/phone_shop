<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PhoneShop</title>
    <link rel="stylesheet" href="./style/homepage.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous" />
</head>

<body>
    <!-- navbar -->
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <!-- navbar-expand-md: when get to medium size screen, show actual navbar. But below medium size screen, show toggle button and drop down-->
        <div class="container-xxl">
            <!-- 100 width until extra extra large -->
            <a href="./index.php" class="navbar-brand">
                <p>PhoneShop</p>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Toggle-navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- navbar-link -->
            <!-- class="data-bs-toggle for_styling_this_div " -->
            <div class="collapse navbar-collapse justify-content-center align-center" id="main-nav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="./phone.php">Phone</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./accessories.php">Accessories</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./about.php">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./contactus.php">Contact us</a>
                    </li>
                    <li class="nav-item d-md-none">
                        <a class="nav-link" href="./LoginForm.php">log in</a>
                    </li>
                    <li class="nav-item d-md-none">
                        <a class="nav-link" href="./signupForm.php">Sign Up</a>
                    </li>
                    <li class="nav-item ms-2 d-none d-md-inline">
                        <!-- ms-2: margin start 2, d-none: on smaller screen, display none, d-md-inline: on medium screen, display inline -->
                        <a class="btn btn-secondary" href="./LoginForm.php">Log In</a>
                    </li>
                    <li class="nav-item ms-2 d-none d-md-inline">
                        <!-- ms-2: margin start 2, d-none: on smaller screen, display none, d-md-inline: on medium screen, display inline -->
                        <a class="btn btn-success" href="./signupForm.php">Sign Up</a>
                    </li>
                    <li class="nav-item ms-2">
                        <?php include("./shoppingCartHeader.php"); ?>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Popular Brands -->
    <section id="brands" class="bg-white">
        <div class="container-lg pt-5">
            <div>
                <h2>POPULAR BRANDS</h2>
            </div>

            <!-- Grid card -->
            <div class="row mt-4">
                <div class="col-2 col-lg-2 col-xl-1">
                    <a href="./brandapple.php">
                        <img src="./images/apple.jpg" class="w-100" alt="">
                    </a>
                </div>
                <div class="col-2 col-lg-2 col-xl-1">
                    <a href="./brandXiaomi.php">
                        <img src="./images/xioami.jpg" class="w-100" alt="">
                    </a>
                </div>
                <div class="col-2 col-lg-2 col-xl-1">
                    <a href="./brandSony.php">
                        <img src="./images/sony.jpg" class="w-100" alt="">
                    </a>
                </div>
                <div class="col-2 col-lg-2 col-xl-1">
                    <a href="./brandAsus.php">
                        <img src="./images/asus.jpg" class="w-100" alt="">
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-2 col-lg-2 col-xl-1">
                    <a href="./brandGoogle.php">
                        <img src="./images/google2.jpg" class="w-100" alt="">
                    </a>
                </div>
                <div class="col-2 col-lg-2 col-xl-1">
                    <a href="./brandHuawei.php">
                        <img src="./images/huawei.jpg" class="w-100" alt="">
                    </a>
                </div>
                <div class="col-2 col-lg-2 col-xl-1 my-2">
                    <a href="./brandLenovo.php">
                        <img src="./images/lenovo.jpg" class="w-100" alt="">
                    </a>
                </div>
                <div class="col-2 col-lg-2 col-xl-1 my-2">
                    <a href="./brandMicrosoft.php">
                        <img src="./images/microsoft.jpg" class="w-100" alt="">
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-2 col-lg-2 col-xl-1 my-2">
                    <a href="./brandOppo.php">
                        <img src="./images/oppo.jpg" class="w-100" alt="">
                    </a>
                </div>
                <div class="col-2 col-lg-2 col-xl-1 my-2">
                    <a href="./brandSamsung.php">
                        <img src="./images/samsung.jpg" class="w-100" alt="">
                    </a>
                </div>
                <div class="col-2 col-lg-2 col-xl-1 my-2">
                    <a href="./brandOneplus.php">
                        <img src="./images/oneplus.jpg" class="w-100" alt="">
                    </a>
                </div>
                <div class="col-2 col-lg-2 col-xl-1 my-2">
                    <a href="./brandVivo.php">
                        <img src="./images/vivo.jpg" class="w-100" alt="">
                    </a>
                </div>
            </div>
        </div>
    </section>

    <?php

    // setup connection to database (databasename = phoneshop)
    $pdo = new PDO('mysql:host=localhost; port=3306; dbname=phoneshop', 'root', '');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $pdo->prepare('SELECT * FROM products WHERE description="new arrival"');
    $statement->execute();
    $products = $statement->fetchAll(PDO::FETCH_ASSOC);

    ?>

    <section id="new_arrival">
        <div class="container-md my-5 bg-light py-5">
            <div>
                <h2>NEW ARRIVAL</h2>
            </div>
            <div class="row row-cols-2 row-cols-md-3 row-cols-lg-4 g-4 mt-1">
                <?php foreach ($products as $i => $product) { ?>
                    <div class="col">
                        <form action="manage_cart2.php" method="POST">
                            <div class="phoneCard card h-100">
                                <img src="./admin/<?php echo $product['image'] ?>" class="card-img-top" alt="..." style="width:10em; margin:auto;" />
                                <div class="card-body text-center">
                                    <h5 class="card-title"><?php echo $product['title'] ?></h5>
                                    <p class="card-text">
                                        The ultimate product arrival.
                                    </p>
                                    <p class="text-danger">$<?php echo $product['price'] ?></p>
                                </div>
                                <button type="submit" name="Add_To_Cart" class="btn btn-success">Add to cart</button>
                                <input type="hidden" name="Item_Name" value="<?php echo $product['title'] ?>">
                                <input type="hidden" name="Price" value="<?php echo $product['price'] ?>">
                            </div>
                        </form>
                    </div>
                <?php } ?>
            </div>
    </section>

    <?php

    // setup connection to database (databasename = phoneshop)
    $pdo = new PDO('mysql:host=localhost; port=3306; dbname=phoneshop', 'root', '');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $pdo->prepare('SELECT * FROM products WHERE description="accessories"');
    $statement->execute();
    $products = $statement->fetchAll(PDO::FETCH_ASSOC);

    ?>

    <section id="accessories">
        <div class="container-md py-5">
            <div>
                <h2>ACCESSORIES</h2>
            </div>
            <div class="row row-cols-2 row-cols-md-3 row-cols-lg-6 g-4 mt-1">
                <?php foreach ($products as $i => $product) { ?>
                    <div class="col">
                        <form action="manage_cart2.php" method="POST">
                            <div class="phoneCard card " style="height: 500px">
                                <img src="./admin/<?php echo $product['image'] ?>" class="card-img-top" alt="..." style="width:10em; margin:auto;" />
                                <div class="card-body text-center">
                                    <h5 class="card-title"><?php echo $product['title'] ?></h5>
                                    <p class="card-text">
                                        The ultimate Accessories.
                                    </p>
                                    <p class="text-danger">$<?php echo $product['price'] ?></p>
                                </div>
                                <button type="submit" name="Add_To_Cart" class="btn btn-primary">Add to cart</button>
                                <input type="hidden" name="Item_Name" value="<?php echo $product['title'] ?>">
                                <input type="hidden" name="Price" value="<?php echo $product['price'] ?>">
                            </div>
                        </form>
                    </div>
                <?php } ?>
            </div>
    </section>

    <?php

    // setup connection to database (databasename = phoneshop)
    $pdo = new PDO('mysql:host=localhost; port=3306; dbname=phoneshop', 'root', '');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $pdo->prepare('SELECT * FROM products WHERE description="special offer"');
    $statement->execute();
    $products = $statement->fetchAll(PDO::FETCH_ASSOC);

    ?>

    <section id="special_offer">
        <div class="container-md py-5 bg-light my-5">
            <div>
                <h2>SPECIAL OFFER</h2>
            </div>
            <div class="row row-cols-2 row-cols-md-3 row-cols-lg-6 g-4 mt-1">
                <?php foreach ($products as $i => $product) { ?>
                    <div class="col">
                        <form action="manage_cart2.php" method="POST">
                            <div class="phoneCard card" style="height: 500px">
                                <img src="./admin/<?php echo $product['image'] ?>" class="card-img-top" alt="..." style="width:10em; margin:auto;" />
                                <div class="card-body text-center">
                                    <h5 class="card-title"><?php echo $product['title'] ?></h5>
                                    <p class="card-text">
                                        The ultimate special offer.
                                    </p>
                                    <p class="border border-primary">$50 off</p>
                                    <p class="text-decoration-line-through">$full price</p>
                                    <p class="text-primary">only $<?php echo $product['price'] ?></p>
                                </div>
                                <button type="submit" name="Add_To_Cart" class="btn btn-danger">Add to cart</button>
                                <input type="hidden" name="Item_Name" value="<?php echo $product['title'] ?>">
                                <input type="hidden" name="Price" value="<?php echo $product['price'] ?>">
                            </div>
                        </form>
                    </div>
                <?php } ?>
            </div>
    </section>

    <section id="footer">
        <div class="container-fluid bg-light py-5">
            <div class="row">
                <div class="col-12">
                    <h3 class="mb-5 text-muted d-flex justify-content-center">FOLLOW US ON</h3>
                    <div class="d-flex justify-content-center">
                        <a href="">
                            <img src="./images/facebook.png" alt="" width="25px" class="me-3">
                        </a>
                        <a href="" class="text-decoration-none">
                            <p>Phone Shop</p>
                        </a>
                    </div>
                    <div class="d-flex justify-content-center">
                        <a href="">
                            <img src="./images/instagram.png" alt="" width="25px" class="me-3">
                        </a>
                        <a href="" href="" class="text-decoration-none">
                            <p>Phone Shop</p>
                        </a>
                    </div>
                    <div class="d-flex justify-content-center">
                        <a href="">
                            <img src="./images/telegram.png" alt="" width="25px" class="me-3">
                        </a>
                        <a href="" class="text-decoration-none">
                            <p>Phone Shop</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid text-center py-3">
            <p>Phone Shop © 2020. All Rights Reserved</p>
        </div>
    </section>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
</body>

</html>