<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <title>Add to cart</title>
</head>

<body>
    <section>
        <div class="container-lg bg-light my-5">
            <h1 class="py-3">Shopping Cart</h1>
            <?php

            // setup connection to database (databasename = phoneshop)
            $pdo = new PDO('mysql:host=localhost; port=3306; dbname=phoneshop', 'root', '');
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $statement = $pdo->prepare('SELECT * FROM shopping_cart');
            $statement->execute();
            $products = $statement->fetchAll(PDO::FETCH_ASSOC);

            ?>
            <div class="container-lg bg-light">
                <?php foreach ($products as $i => $product) { ?>
                    <div class="col">
                        <div class="phoneCard card h-100">
                            <img src="./admin/<?php echo $product['image'] ?>" class="card-img-top" alt="..." style="width:10em; margin:auto;" />
                            <div class="card-body text-center">
                                <h5 class="card-title"><?php echo $product['title'] ?></h5>
                                <p class="card-text">
                                    The ultimate product arrival.
                                </p>
                                <p class="text-danger">$<?php echo $product['price'] ?></p>
                                <p class="text-success">Quantity: <?php echo $product['qty'] ?></p>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div class="py-4 text-center">
                    <h4>Subtotal (<?php $item ?> items): <?php $total_price ?></h4>
                </div>
            </div>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
        </div>
    </section>
</body>

</html>