<?php

// connect to database
$pdo = new PDO('mysql:host=localhost; port=3306; dbname=phoneshop', 'root', '');
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$errors = [];
$email = '';
$password = '';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $email = $_POST['email'];
  $password = $_POST['password'];

  if (!$email) {
      $errors[] = "Email is required*";
  }
  if (!$password) {
      $errors[] = "Password is required*";
  }
  if (empty($errors)) {
    $stmt = $pdo->query("SELECT * FROM users");
    $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

    foreach($users as $i => $user) {
      if($user['email'] == $email && $user['password'] == $password) {
        header('location: ./admin/index.php');
      }
    }
    $errors[] = "Invalid username or password";
  }
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Login Form</title>

    <!-- CSS -->
    <link rel="stylesheet" href="./style/loginSignup.css" />

    <!-- Boxicons CSS -->
    <link
      href="https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css"
      rel="stylesheet"
    />
    
    
  </head>
  <body>
    <section class="container forms">
      <!-- Login Form -->
      <div class="form login">
        <div class="form-content">
          <header>Login</header>

          <!-- alert error -->
          <?php
          if(!empty($errors)) { ?>
          <div class="alert">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
            <?php foreach ($errors as $error) { ?>
                <div> <?php echo $error; ?> </div>
            <?php } ?>
          </div>
          <?php } ?>

          <form action="" method="post">
            <div class="field input-field">
              <input type="email" placeholder="Email" class="input" name="email" />
            </div>

            <div class="field input-field">
              <input type="password" placeholder="Password" class="password" name="password"/>
              <i class='bx bx-hide eye-icon'></i>
            </div>

            <div class="form-link">
              <a href="#" class="forgot-pass">Forgot Password?</a>
            </div>

            <div class="field button-field">
              <button type="submit">Login</button>
            </div>
          </form>

          <div class="form-link">
            <span
              >Already have an account?
              <a href="./signupForm.php" class="link signup-link">Signup</a>
            </span>
          </div>
        </div>

        <div class="line"></div>

        <div class="media-options">
          <a href="https://web.facebook.com/login.php/?_rdc=1&_rdr" class="field facebook">
            <i class='bx bxl-facebook facebook-icon'></i>
            <span>Login with Facebook</span>
          </a>
        </div>

        <div class="media-options">
          <a href="https://accounts.google.com/v3/signin/identifier?dsh=S-805132891%3A1675273087075864&continue=https%3A%2F%2Fmyaccount.google.com%3Futm_source%3Daccount-marketing-page%26utm_medium%3Dcreate-account-button&flowName=GlifWebSignIn&flowEntry=AddSession" class="field google">
            <img src="../images/google.png" alt="" class="google-img">
            <span>Login with Google</span>
          </a>
        </div>
      </div>
    </section>

    <!-- Javascript -->
    <script src="./js/script.js"></script>

  </body>
</html>
