<?php include("./header.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>cart</title>
</head>

<body>
    <section id="myCart">
        <div class="container-md my-5 py-5">
            <div class="text-center bg-success text-white py-3 mb-5">
                <h2>MY CART</h2>
            </div>
            <div class="row">
                <div class="col-lg-9">
                    <table class="table">
                        <thead class="text-center">
                            <tr>
                                <th scope="col">Serial No.</th>
                                <th scope="col">Item Name</th>
                                <th scope="col">Item Price</th>
                                <th scope="col">Quantity</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <?php
                            $total = 0;
                            if (isset($_SESSION['cart'])) {
                                foreach ($_SESSION['cart'] as $key => $value) {
                                    $sr = $key + 1;
                                    $total = $total + $value['Price'];
                                    //print_r($value);
                                    echo "
                                    <tr>
                                        <td>$sr</td>
                                        <td>$value[Item_Name]</td>
                                        <td>$value[Price]</td>
                                        <td>
                                            <input class='text-center' type='number' value='$value[Quantity]' min='1'> 
                                        </td>
                                        <td>
                                            <form action='manage_cart2.php' method='POST'>
                                                <button name='Remove_Item' class='btn btn-sm btn-outline-danger'>REMOVE</button>
                                                <input type='hidden' name='Item_Name' value='$value[Item_Name]'>
                                            </form>
                                        </td>
                                    </tr>
                                    ";
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-3">
                    <div class="border bg-light rounded p-4">
                        <h4>Total:</h4>
                        <h5 class="text-end"><?php echo $total ?></h5>
                        <br>
                        <form>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked>
                                <label class="form-check-label" for="flexRadioDefault2">
                                    Cash on Delivery
                                </label>
                            </div>
                            <br>
                            <button class="btn btn-primary btn-block">Make Purchase</button>
                        </form>
                    </div>
                </div>
            </div>
    </section>

    <?php include("./footer.php") ?>
</body>

</html>