<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Phone</title>
    <link rel="stylesheet" href="./style/homepage.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous" />
</head>

<body>
    <!-- navbar -->
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <!-- navbar-expand-md: when get to medium size screen, show actual navbar. But below medium size screen, show toggle button and drop down-->
        <div class="container-xxl">
            <!-- 100 width until extra extra large -->
            <a href="./index.php" class="navbar-brand">
                <p>PhoneShop</p>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Toggle-navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- navbar-link -->
            <!-- class="data-bs-toggle for_styling_this_div " -->
            <div class="collapse navbar-collapse justify-content-center align-center" id="main-nav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="./phone.php">Phone</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./accessories.php">Accessories</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./about.php">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./contactus.php">Contact us</a>
                    </li>
                    <li class="nav-item d-md-none">
                        <a class="nav-link" href="./LoginForm.php">log in</a>
                    </li>
                    <li class="nav-item d-md-none">
                        <a class="nav-link" href="./signupForm.php">Sign Up</a>
                    </li>
                    <li class="nav-item ms-2 d-none d-md-inline">
                        <!-- ms-2: margin start 2, d-none: on smaller screen, display none, d-md-inline: on medium screen, display inline -->
                        <a class="btn btn-secondary" href="./LoginForm.php">Log In</a>
                    </li>
                    <li class="nav-item ms-2 d-none d-md-inline">
                        <!-- ms-2: margin start 2, d-none: on smaller screen, display none, d-md-inline: on medium screen, display inline -->
                        <a class="btn btn-success" href="./signupForm.php">Sign Up</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <?php
    // setup connection to database (databasename = phoneshop)
    $pdo = new PDO('mysql:host=localhost; port=3306; dbname=phoneshop', 'root', '');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $pdo->prepare('SELECT * FROM products WHERE category="Asus"');
    $statement->execute();
    $products = $statement->fetchAll(PDO::FETCH_ASSOC);

    ?>

    <section id="Asus">
        <div class="container-md my-5 bg-light py-5">
            <div>
                <h2>Asus</h2>
            </div>
            <div class="row row-cols-2 row-cols-md-3 row-cols-lg-4 g-4 mt-1">
                <?php foreach ($products as $i => $product) { ?>
                    <div class="col">
                        <div class="phoneCard card h-100">
                            <img src="./admin/<?php echo $product['image'] ?>" class="card-img-top" alt="..." style="width:10em; margin:auto;" />
                            <div class="card-body text-center">
                                <h5 class="card-title"><?php echo $product['title'] ?></h5>
                                <p class="card-text">
                                    The ultimate product arrival.
                                </p>
                                <p class="text-danger">$<?php echo $product['price'] ?></p>
                            </div>
                            <a href="./addToCart.php?id=<?php echo $product['id']; ?>" class="add_to_cart btn btn-primary">Add to cart</a>
                        </div>
                    </div>
                <?php } ?>
            </div>
    </section>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
</body>

</html>