<?php

// setup connection to database (databasename = phoneshop)
$pdo = new PDO('mysql:host=localhost; port=3306; dbname=phoneshop', 'root', '');
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $statement = $pdo->prepare("SELECT * FROM products WHERE id=:id");
    $statement->execute(['id' => $id]);
    $product = $statement->fetch();
}


$errors = [];
$title = '';
$category = '';
$description = '';
$price = '';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $title = $_POST['title'];
    $category = $_POST['category'];
    $description = $_POST['description'];
    $price = $_POST['price'];

    if (!$title) {
        $errors[] = "Product Title is required*";
    }
    if (!$category) {
        $errors[] = "Product Category is required*";
    }
    if (!$price) {
        $errors[] = "Product Price is required*";
    }

    // if have no images folder, create folder name images
    if (!is_dir('images')) {
        mkdir('images');
    }

    if (empty($errors)) {
        // upload file
        $image = $_FILES['image']['name'] ? $_FILES['image']['name'] : $product['image'];
        $imagePath = '';
        // var_dump(file_exists($image), $image, $product['image']);
        // die();
        // check image exist or not
        if (!file_exists($image)) {
            $imagePath = 'images/' . randomString(8) . '/' . $_FILES['image']['name'];
            mkdir(dirname($imagePath));
            move_uploaded_file($_FILES['image']['tmp_name'], $imagePath);
        } else {
            $imagePath = $image;
        }
        /*         $data = [
            "title"       => $title,
            "image"       => $imagePath,
            "description" => $description,
            "price"       => $price,
            "updated_at"  => $date,
            "id"          => $id
        ]; */
        $statement = $pdo->prepare("UPDATE 
            products SET title=:title,
            category=:category,
            image=:image,
            description=:description,
            price=:price    
            WHERE id=:id");

        $statement->bindValue(':title', $title);
        $statement->bindValue(':category', $category);
        $statement->bindValue(':image', $imagePath);
        $statement->bindValue(':description', $description);
        $statement->bindValue(':price', $price);
        $statement->bindValue(':id', $id);
        // $statement->execute($data);
        $statement->execute();

        // redirect to file index.php
        header('Location: index.php');
    }
}

// get random character
function randomString($n)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $str = '';
    for ($i = 0; $i < $n; $i++) {
        $index = rand(0, strlen($characters) - 1);
        $str .= $characters[$index];
    }
    return $str;
}

?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Edit product</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="./../style/app.css"> -->
    <style>
        body {
            height: 100vh;
            width: 100%;
            background-color: #4070f4;

        }

        .container {
            background-color: #fff;
            margin-bottom: 10px;
        }

        .thumb-image {
            width: 5rem;
        }

        .update-image {
            width: 120px;
        }
    </style>
</head>

<body>
    <div class="container">
        <h1>Edit Product</h1>

        <?php if (!empty($errors)) { ?>
            <div class="alert alert-danger">
                <?php foreach ($errors as $error) { ?>
                    <div> <?php echo $error; ?> </div>
                <?php } ?>
            </div>
        <?php } ?>
        <form action="" method="post" enctype="multipart/form-data"> <!-- enctype="multipart/form-data": use for file upload.e.g. image -->
            <?php if ($product['image']) : ?>
                <img src="<?php echo $product['image']; ?>" class="update-image" alt="">
            <?php endif; ?>
            <div class="form-group py-2">
                <label for="InputPTitle">Product Title</label>
                <input type="text" name="title" value="<?php echo $product["title"] ?>" class="form-control border border-primary">
            </div>
            <div class="form-group py-2">
                <label for="InputPImage">Product Image</label>
                <input type="file" name="image" class="form-control border border-primary">
            </div>
            <div class="form-group py-2">
                <label for="InputPCategory">Product Category</label>
                <select name="category" id="category" class="form-control border border-primary">
                    <option value="<?php echo $category ?>"> <?php echo $product['category'] ?> </option>
                    <option value="Apple">Apple</option>
                    <option value="Samsung">Samsung</option>
                    <option value="Oppo">Oppo</option>
                    <option value="OnePlus">OnePlus</option>
                    <option value="Huawei">Huawei</option>
                    <option value="Xiaomi">Xiaomi</option>
                    <option value="JBL">JBL</option>
                    <option value="Sony">Sony</option>
                    <option value="Asus">Asus</option>
                    <option value="Google">Google</option>
                    <option value="Huawei">Huawei</option>
                    <option value="Lenovo">Lenovo</option>
                    <option value="Microsoft">Microsoft</option>
                    <option value="Oppo">Oppo</option>
                    <option value="Vivo">Vivo</option>
                </select>
            </div>
            <div class="form-group py-2">
                <label for="InputPDescription">Product Description</label>
                <textarea class="form-control border border-primary" name="description"><?php echo $product["description"] ?></textarea>
            </div>
            <div class="form-group py-2">
                <label for="InputPPrice">Product Price</label>
                <input type="number" step=".01" name="price" value="<?php echo $product["price"] ?>" class="form-control border border-primary">
            </div>
            <div class="form-group py-2">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="index.php" class="btn btn-danger">Back</a>
            </div>
        </form>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
</body>

</html>