<?php

// setup connection to database (databasename = phoneshop)
$pdo = new PDO('mysql:host=localhost; port=3306; dbname=phoneshop', 'root', '');
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$statement = $pdo->prepare('SELECT * FROM products ORDER BY create_date DESC');
$statement->execute();
$products = $statement->fetchAll(PDO::FETCH_ASSOC);


?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Product</title>
    <!-- <link rel="stylesheet" href="./../style/app.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <style>
        body {
            height: 100vh;
            width: 100%;
            background-color: #4070f4;

        }
        .container {
            background-color: #fff;
            margin-bottom: 10px;
        }

        .thumb-image {
            width: 5rem;
        }

        .update-image {
            width: 120px;
        }
    </style>
</head>

<body>
    <div class="container">
        <h3 class="text-primary">Phone Shop</h3>
        <p class="float-end">
            <a href="create.php" class="btn btn-success">Create Product</a>
        </p>

        <table class="table table-bordered border-primary">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Image</th>
                    <th scope="col">Title</th>
                    <th scope="col">Category</th>
                    <th scope="col">Price</th>
                    <th scope="col">Description</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($products as $i => $product) { ?>
                    <tr>
                        <th scope="row"><?php echo $i + 1; ?></th>
                        <td>
                            <img src="./<?php echo $product['image'] ?>" alt="phone" class="thumb-image">
                        </td>
                        <td><?php echo $product['title'] ?></td>
                        <td><?php echo $product['category'] ?></td>
                        <td>$<?php echo $product['price'] ?></td>
                        <td><?php echo $product['description'] ?></td>
                        <td>
                            <a href="./edit.php?id=<?php echo $product['id'] ?>" type="button" class="btn btn-sm btn-outline-primary">Edit</a>
                            <a href="./delete.php?id=<?php echo $product['id']; ?>" type="button" class="btn btn-sm btn-outline-danger">Delete</a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
</body>

</html>