<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About Us</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous" />
</head>

<body>
    <!-- navbar -->
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <!-- navbar-expand-md: when get to medium size screen, show actual navbar. But below medium size screen, show toggle button and drop down-->
        <div class="container-xxl">
            <!-- 100 width until extra extra large -->
            <a href="./index.php" class="navbar-brand">
                <p>PhoneShop</p>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Toggle-navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- navbar-link -->
            <!-- class="data-bs-toggle for_styling_this_div " -->
            <div class="collapse navbar-collapse justify-content-center align-center" id="main-nav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="./phone.php">Phone</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./accessories.php">Accessories</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./about.php">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./contactus.php">Contact us</a>
                    </li>
                    <li class="nav-item d-md-none">
                        <a class="nav-link" href="./LoginForm.php">log in</a>
                    </li>
                    <li class="nav-item d-md-none">
                        <a class="nav-link" href="./signupForm.php">Sign Up</a>
                    </li>
                    <li class="nav-item ms-2 d-none d-md-inline">
                        <!-- ms-2: margin start 2, d-none: on smaller screen, display none, d-md-inline: on medium screen, display inline -->
                        <a class="btn btn-secondary" href="./LoginForm.php">Log In</a>
                    </li>
                    <li class="nav-item ms-2 d-none d-md-inline">
                        <!-- ms-2: margin start 2, d-none: on smaller screen, display none, d-md-inline: on medium screen, display inline -->
                        <a class="btn btn-success" href="./signupForm.php">Sign Up</a>
                    </li>
                    <li class="nav-item ms-2">
                        <?php include("./shoppingCartHeader.php"); ?>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <section id="body">
        <div class="container-fluid mb-5 pb-5">
            <!-- <div class="text-center text-white bg-secondary py-4">
                <h1>About Us</h1>
                <p class="lead">The Perfect Phone Online Shop </p>
            </div> -->
            <h1 class="text-center text-dark bg-white py-3">Our team</h1>
            <div class="row row-cols-1 row-cols-md-2 row-cols-lg-5 g-3">
                <div class="col">
                    <div class="card h-100">
                        <img src="./images/memberPhoto/der.jpg" alt="" class="card-img-top" style="height:300px; object-fit:cover;">
                        <div class="card-body">
                            <h4 class="card-title">Chea Sunder</h4>
                            <p class="card-text text-muted">
                                About Us page Designer
                            </p>
                            <p class="card-text">sunder44@gmail.com</p>
                        </div>
                        <a href="https://www.facebook.com/nhomram.bosk?mibextid=LQQJ4d" class="contact btn btn-dark">Contact</a>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <img src="./images/memberPhoto/nich.jpg" alt="" class="card-img-top" style="height:300px; object-fit:cover;">
                        <div class="card-body">
                            <h4 class="card-title">Boeun Bonich</h4>
                            <p class="card-text text-muted">
                                Contact Us page Designer
                            </p>
                            <p class="card-text">bonich22@gmail.com</p>
                        </div>
                        <a href="https://www.facebook.com/bonich.pich?mibextid=LQQJ4d" class="contact btn btn-dark">Contact</a>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <img src="./images/memberPhoto/rin3.JPG" alt="" class="card-img-top" style="height:300px; object-fit:cover;">
                        <div class="card-body">
                            <h4 class="card-title">Horn Narin</h4>
                            <p class="card-text text-muted">
                                Homepage Designer and Project Analyst
                            </p>
                            <p class="card-text">hornnarin600@gmail.com</p>
                        </div>
                        <a href="https://www.facebook.com/triple.hdx.587?mibextid=LQQJ4d" class="contact btn btn-dark">Contact</a>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <img src="./images/memberPhoto/reay.JPG" alt="" class="card-img-top" style="height:300px; object-fit:cover;">
                        <div class="card-body">
                            <h4 class="card-title">Pe Punreay</h4>
                            <p class="card-text text-muted">
                                Accessories page Designer
                            </p>
                            <p class="card-text">punreaype0@gmail.com</p>
                        </div>
                        <a href="https://www.facebook.com/pe.punreay?mibextid=LQQJ4d" class="contact btn btn-dark">Contact</a>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <img src="./images/memberPhoto/run.JPG" alt="" class="card-img-top" style="height:300px; object-fit:cover;">
                        <div class="card-body">
                            <h4 class="card-title">Sem Puthearun</h4>
                            <p class="card-text text-muted">
                                Phone page Designer
                            </p>
                            <p class="card-text">puthearun33@@gmail.com</p>
                        </div>
                        <a href="https://www.facebook.com/runtesiro.sem?mibextid=LQQJ4d" class="contact btn btn-dark">Contact</a>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <img src="./images/memberPhoto/dom.PNG" alt="" class="card-img-top" style="height:300px; object-fit:cover;">
                        <div class="card-body">
                            <h4 class="card-title">Tith Oudom</h4>
                            <p class="card-text text-muted">
                                Login page and database Designer
                            </p>
                            <p class="card-text">tithoudom@gmail.com</p>
                        </div>
                        <a href="https://www.facebook.com/profile.php?id=100020986424162&mibextid=LQQJ4d" class="contact btn btn-dark">Contact</a>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <img src="./images/memberPhoto/sopheak.jpg" alt="" class="card-img-top" style="height:300px; object-fit:cover;">
                        <div class="card-body">
                            <h4 class="card-title">Phat Sopheak</h4>
                            <p class="card-text text-muted">
                                Signup page Designer
                            </p>
                            <p class="card-text">phatsopheak66@gmail.com</p>
                        </div>
                        <a href="https://t.me/sopheakbka" class="contact btn btn-dark">Contact</a>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <img src="./images/memberPhoto/tem.jpg" alt="" class="card-img-top" style="height:300px; object-fit:cover;">
                        <div class="card-body">
                            <h4 class="card-title">Tuo Tem</h4>
                            <p class="card-text text-muted">
                                List Product add edit and delete page Designer
                            </p>
                            <p class="card-text">tuotem77@gmail.com</p>
                        </div>
                        <a href="https://t.me/tuotem" class="contact btn btn-dark">Contact</a>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <img src="./images/memberPhoto/seth.jpg" alt="" class="card-img-top" style="height:300px; object-fit:cover;">
                        <div class="card-body">
                            <h4 class="card-title">thoeurn Seth</h4>
                            <p class="card-text text-muted">
                                brand Apple Designer
                            </p>
                            <p class="card-text">thoeurnseth88@gmail.com</p>
                        </div>
                        <a href="https://t.me/thoeurnseth" class="contact btn btn-dark">Contact</a>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <img src="./images/memberPhoto/somay.jpg" alt="" class="card-img-top" style="height:300px; object-fit:cover;">
                        <div class="card-body">
                            <h4 class="card-title">Non Chamnabbunsunsamay</h4>
                            <p class="card-text text-muted">
                                brand Sony Designer
                            </p>
                            <p class="card-text">nonchamnabbunsunsamay99@gmail.com</p>
                        </div>
                        <a href="https://t.me/Non_chamnabbunsunsamay" class="contact btn btn-dark">Contact</a>
                    </div>
                </div>
                <div class="col">
                    <div class="card h-100">
                        <img src="./images/memberPhoto" alt="" class="card-img-top" style="height:300px; object-fit:cover;">
                        <div class="card-body">
                            <h4 class="card-title">Khorn Sokha</h4>
                            <p class="card-text text-muted">
                                Brand Asus Designer
                            </p>
                            <p class="card-text">khornsokha123@gmail.com</p>
                        </div>
                        <a href="https://t.me/Naa_Kha" class="contact btn btn-dark">Contact</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="footer">
        <div class="container-fluid bg-light py-5">
            <div class="row">
                <div class="col-12">
                    <h3 class="mb-5 text-muted d-flex justify-content-center">FOLLOW US ON</h3>
                    <div class="d-flex justify-content-center">
                        <a href="">
                            <img src="./images/facebook.png" alt="" width="25px" class="me-3">
                        </a>
                        <a href="" class="text-decoration-none">
                            <p>Phone Shop</p>
                        </a>
                    </div>
                    <div class="d-flex justify-content-center">
                        <a href="">
                            <img src="./images/instagram.png" alt="" width="25px" class="me-3">
                        </a>
                        <a href="" href="" class="text-decoration-none">
                            <p>Phone Shop</p>
                        </a>
                    </div>
                    <div class="d-flex justify-content-center">
                        <a href="">
                            <img src="./images/telegram.png" alt="" width="25px" class="me-3">
                        </a>
                        <a href="" class="text-decoration-none">
                            <p>Phone Shop</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid text-center py-3">
            <p>Phone Shop © 2020. All Rights Reserved</p>
        </div>
    </section>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
</body>

</html>